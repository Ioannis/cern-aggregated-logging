#!/bin/bash

# a script to send fluentd events from a buffer file to Central Monitoring
# using HTTP submission (cf. http://monitdocs.web.cern.ch/monitdocs/ingestion/service_logs.html#http)
# The fluentd buffer file contains json-formatted events, one per line.
# All we need is to merge them into a json array and submit them to the Central Monitoring HTTP endpoint.

monit_endpoint=${MONIT_HTTP_ENDPOINT:-http://monit-logs.cern.ch:10012/}
buffer_file=$1

# use a small ruby script (fluentd itself is Ruby) to turn the buffer file containing one json-formatter
# event per line into a json array: add surrounding [ ] and prefix second and subsequent lines with a coma.
# We need to remove trailing newlines (chomp) to keep the CERN monit endpoint happy.
ruby -e 'print "["; while gets do print "," if $. > 1; print $_.chomp end; print "]";' $buffer_file | \
    curl --fail --silent -H 'Content-Type: application/json; charset=UTF-8' --data-binary @- "${monit_endpoint}"
